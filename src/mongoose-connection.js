import mongoose, { mongo } from "mongoose"
import config from "./config"

mongoose.Promise = global.Promise
let primaryHost = `${config.mongo.primaryHost}:${config.mongo.port}`
let secondary0Host = config.mongo.secondary0Host?`,${config.mongo.secondary0Host}:${config.mongo.port}`:""
let secondary1Host = config.mongo.secondary1Host?`,${config.mongo.secondary1Host}:${config.mongo.port}`:""
console.log(config.mongo.replicaSet)
let mongodbConnectionURL = `mongodb://${config.mongo.user}:${config.mongo.pwd}@${primaryHost}${secondary0Host}${secondary1Host}/${config.mongo.db}?replicaSet=${config.mongo.replicaSet}`
// let mongodbConnectionURL = `mongodb://${primaryHost}${secondary0Host}${secondary1Host}/${config.mongo.db}`

console.log(mongodbConnectionURL)
var options = {
	useMongoClient:true,
	user:config.mongo.user,
	pass:config.mongo.pwd,
	// replicaSet:config.mongo.replicaSet
}
if (config.mongo.replicaSet) {
	options.replicaSet = config.mongo.replicaSet
}
export default mongoose.createConnection(mongodbConnectionURL)