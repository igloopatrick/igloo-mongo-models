import mongooseConnection from "./mongoose-connection"
import mongoose from "mongoose"

let Schema = mongoose.Schema

export default mongooseConnection.model("OAuthClient", new Schema({
	// clientId: { type: String },
	name:String,
	secret: { type: String },
	redirectUris: [String],
	grants: [String]
}))