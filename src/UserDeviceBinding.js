import mongoose from "mongoose"
import mongooseConnection from "./mongoose-connection"

var Schema = mongoose.Schema

var MobileDeviceSchema = new Schema({
	name: String,
	osName: String,
	osVersion: String,
	brand: String,
	model: String,
	UUID: String,
	FCMRegistrationToken: String,
	recieveNotification:Boolean
})

var DeviceSchema = new Schema({
	friendlyName: String,
	channel: Number,
	baseId: Number,
	bindedWifiGateway: String,
	applianceId: String,
	deviceType: { type: Schema.Types.ObjectId, ref: "DeviceType" }

})

var UserDeviceBindingSchema = new Schema({
	userId: Number,
	_user: {type: Schema.Types.ObjectId, ref:"User"},
	mqttUsername: String,
	mqttPassword: String,
	FCMGroup: {
		notificationKey: String
	}, // Mobile Device Schema
	
	devices: { type: [DeviceSchema], default: [] }
})

function hasDevice(devices, applianceId) {
	// console.log(devices)
	for (var index in devices) {
		if (devices[index].applianceId == applianceId) {
			// console.log(true)
			return true
		}
	}
	return false
}

function hasFriendlyName(devices, friendlyName) {
	// console.log(devices)
	for (var index in devices) {
		if (devices[index].friendlyName == friendlyName) {
			// console.log(true)
			return true
		}
	}
	return false
}

UserDeviceBindingSchema.methods.createOrUpdate = function () {
	return new Promise((resolve, reject) => {
		// console.log(this.UserId)
		this.model("UserDeviceBinding").find({ userId: this.userId }, (error, userDeviceBindings) => {
			if (error) return reject(error)
			if (userDeviceBindings.length != 0) {
				// for (var device in this.devices) {
				// }
				var userDeviceBinding = userDeviceBindings[0]
				if (hasFriendlyName(userDeviceBinding.devices, this.devices[0].friendlyName)) {
					return reject(new Error(`User have a device named ${this.devices[0].friendlyName}`))
				}

				if (!hasDevice(userDeviceBinding.devices, this.devices[0].applianceId)) {
					// console.log(userDeviceBinding.devices)
					userDeviceBinding.devices.push(this.devices[0])
				}
				else {
					return reject(new Error(`User has already enabled ${this.devices[0].applianceId}.`))
				}
				userDeviceBinding.save((error, updatedUserDeviceBinding) => {
					if (error) return reject(error)
					resolve(updatedUserDeviceBinding)
				})
			}
			else {
				this.save((error, result) => {
					if (error) return reject(error)
					resolve(result)
				})
			}
		})
	})
}

UserDeviceBindingSchema.methods.updateDeviceFieldsByDeviceBindingId = function(_id,values) {
	return new Promise((resolve,reject) => {
		var devices = this.devices
		// console.log(devices)
		let friendlyName = values.friendlyName
		let bindedWifiGateway = values.bindedWifiGateway
	
		if (friendlyName) {
			if (hasFriendlyName(device,friendlyName)) {
				return reject(new Error(`User have a device named ${friendlyName}`))
			}
		}
		var device
		// console.log(devices.[index])
		for (var index in devices) {
			
			
			if (devices[index]._id && devices[index]._id.toString() === _id) {
				console.log(devices[index]._id.toString())
				device = devices[index]
				if (friendlyName) {
					this.devices[index].friendlyName = friendlyName
				}
				if (bindedWifiGateway) {
					this.devices[index].bindedWifiGateway = bindedWifiGateway
				}
				this.save()
					.then(newRecord => {
						resolve(newRecord)
					})
					.catch(error=>{
						return reject(error)
					})
			}
		}
		if (!device) {
			return reject(new Error(`Binding: ${_id} does not exist.`))
		}

		
	})
	
	
}

UserDeviceBindingSchema.statics.findByUserIdAndApplianceId = function (userId, applianceId) {

	// this.model("DeviceType").findOne({ prefix: prefix }, (error, deviceType) => {
	return new Promise((resolve, reject) => {
		console.log(applianceId)
		this.find({ userId: userId }, { devices: { $elemMatch: { applianceId: applianceId } } }).populate("devices.deviceType").exec((error, userDeviceBindings) => {

			if (error) return reject(error)
			console.log(userDeviceBindings)
			resolve(userDeviceBindings[0])
		})
	})
}

UserDeviceBindingSchema.statics.findByUserId = function (userId) {

	// this.model("DeviceType").findOne({ prefix: prefix }, (error, deviceType) => {
	return new Promise((resolve, reject) => {
		this.findOne({ userId: userId },(error, userDeviceBinding) => {

			if (error) return reject(error)
			// console.log(userDeviceBinding)
			resolve(userDeviceBinding)
		})
	})
}

UserDeviceBindingSchema.statics.findAllByUserId = function (userId) {

	// this.model("DeviceType").findOne({ prefix: prefix }, (error, deviceType) => {
	return new Promise((resolve, reject) => {
		this.find({ userId: userId }).populate("devices.deviceType").exec((error, userDeviceBindings) => {

			if (error) return reject(error)
			// console.log(userDeviceBindings)
			resolve(userDeviceBindings[0])
		})
	})
}

UserDeviceBindingSchema.statics.updateDevices = function (userId) {

	// this.model("DeviceType").findOne({ prefix: prefix }, (error, deviceType) => {
	return new Promise((resolve, reject) => {
		this.find({ userId: userId }).populate("devices.deviceType").exec((error, userDeviceBindings) => {

			if (error) return reject(error)
			console.log(userDeviceBindings)
			resolve(userDeviceBindings[0])
		})
	})
}



// Validators

function updateDeviceNameValidator(value,response) {

}

function UUIDUniqueValidator(value, response) {

}

var UserDeviceBinding = mongooseConnection.model("UserDeviceBinding", UserDeviceBindingSchema)

export default UserDeviceBinding