import mongooseConnection from "./mongoose-connection"
import mongoose from "mongoose"

var Schema = mongoose.Schema

var UserSchema = new Schema({
	id: Number,
	name: String,
	email: String,
	password: String,
	username: String,
	createdAt: Date,
	updatedAt: Date,
	mqtt:
	{
		username: String,
		encrypt_username: String,
		otherEncryptPassword: String,
		password: String,
		usernameIv: Buffer,
		otherEncryptPasswordIV: Buffer,
		// updated_at: 2016-11-17T08:34:28.000Z },
	},
	_mqttUser:{type: Schema.Types.ObjectId, ref: "MqttUser"},
	wifiDevices:[{type: Schema.Types.ObjectId,ref: "Device"}],
	bluetoothDevices:[{type: Schema.Types.ObjectId,ref: "Device"}],
	wifiGateways:[{type: Schema.Types.ObjectId,ref: "Device"}],
	amazon:
	{
		amazonUserId: String,
		amazonEmail: String
	}
})

var User = mongooseConnection.model("User", UserSchema)

export default User