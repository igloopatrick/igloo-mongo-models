import mongooseConnection from "./mongoose-connection"
import mongoose from "mongoose"

let Schema = mongoose.Schema

let MqttUserSchema = Schema({
	username: String,
	password: String,
	type: String,
	encryptUsername: String,
	usernameIV: Buffer,
	otherEncryptPassword: String,
	otherEncryptPasswordIV: Buffer,
	publish: [String],
	subscribe: [String],
	pubsub: [String],
	createdAt: Date,
	updatedAt: Date
})
/** Check if the topic is in the pubsub array
 * @param  {String} topic ACL topic string
 * @return {boolean} if topic is in the array
 */
MqttUserSchema.methods.aclTopicExists = function (topic) {
	var exist = false
	this.pubsub.forEach(topicElement => {
		if (topicElement == topic) {
			exist = true
		}
	})
	return exist
}

let MqttUser = mongooseConnection.model("MqttUser", MqttUserSchema)

export default MqttUser