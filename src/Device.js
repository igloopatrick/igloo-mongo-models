import mongoose from "mongoose"
import mongooseConnection from "./mongoose-connection"

var Schema = mongoose.Schema

var deviceSchema = new Schema({
	name: {
		type: String,
		unique: true
	},
	mac: String,
	_deviceType: { type: Schema.Types.ObjectId, ref: "DeviceType" },
	_deviceModel: { type: Schema.Types.ObjectId, ref: "DeviceModel" },
	_orderId: { type: Schema.Types.ObjectId, ref: "Order" },
	importTime: Date,
	currentSoftwareVersion: String
})

var Device = mongooseConnection.model("Device", deviceSchema)

export default Device