import dotenv from "dotenv"
dotenv.config()

export default {
	mongo: {
		primaryHost: process.env.MODE == "PROD" ? process.env.MONGO_HOST : process.env.MONGO_DEV_HOST,
		secondary0Host: process.env.MODE == "PROD" ? process.env.MONGO_SECONDARY_0 : process.env.MONGO_SECONDARY_0_DEV,
		secondary1Host:process.env.MODE == "PROD" ? process.env.MONGO_SECONDARY_1 : process.env.MONGO_SECONDARY_1_DEV,
		replicaSet: process.env.MODE == "PROD" ? process.env.MONGO_REPLICA_SET : process.env.MONGO_REPLICA_SET_DEV,
		port: process.env.MODE == "PROD" ? process.env.MONGO_PORT : process.env.MONGO_DEV_PORT,
		user: process.env.MODE == "PROD" ? process.env.MONGO_USER : process.env.MONGO_DEV_USER,
		pwd: process.env.MODE == "PROD" ? process.env.MONGO_PWD : process.env.MONGO_DEV_PWD,
		db: process.env.MODE == "PROD" ? process.env.MONGO_DB : process.env.MONGO_DEV_DB,
	}
}