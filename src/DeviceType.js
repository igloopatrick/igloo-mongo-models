import mongoose from "mongoose"
import mongooseConnection from "./mongoose-connection"

var Schema = mongoose.Schema
// var alexaCapabilitiesSchema = 
var deviceTypeSchema = new Schema({
	applianceTypes: [String],
	prefix: [String],// Not required by Alexa
	name:String,
	description: String,
	productTypeIdentifier:String,
	manufacturer: String,
	model: String, // modelName
	version: String,
	actions: [String],
	communicationType: String,
	targetDeviceType:String,
	handler:String,
	models:[
		{type: Schema.Types.ObjectId, ref: "DeviceModel"}
	],
	alexa:{
		enabled:{type:Boolean, default:false},
		displayCategories:[String],
		capabilities:[
			{
				type:{type:String},
				interface:String,
				version:String
			}
		]
	}
})

deviceTypeSchema.statics.findByPrefix = function (prefix) {
	// this.model("DeviceType").findOne({ prefix: prefix }, (error, deviceType) => {
	return new Promise((resolve, reject) => {
		// console.log("In findByPrefix")
		this.find({ prefix: {$all:[prefix]} }, (error, deviceTypes) => {
			if (error) return reject(error)
			// console.log(deviceTypes)
			resolve(deviceTypes[0])
		})
	})
}

var DeviceType = mongooseConnection.model("DeviceType", deviceTypeSchema)

export default DeviceType