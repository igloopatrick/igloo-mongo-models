import mongoose from "mongoose"
import mongooseConnection from "./mongoose-connection"

var Schema = mongoose.Schema

var schedulerSchema = new Schema({
	name:String,
	data:{
		_user:Schema.Types.ObjectId,
		cron:String,
		timezone:String,
		type:String,
		deviceLabel:String,
		deviceName:String,
		httpCommand:Schema.Types.Mixed,
		topic:String,
		Body:Schema.Types.Mixed
	}	
})

