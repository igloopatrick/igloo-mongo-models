import User from "../src/user"
import should from "should"
import MongoConnection from "../src/mongoose-connection"

import dotenv from "dotenv"

dotenv.config()

let mode = process.env.MODE
describe("User", () => {
	it("User.findById()", (done) => {

		var _id
		console.log(mode)
		if (mode === "DEV") {
			_id = "599539045d3d47488877538b"
		}
		else {
			_id = "599539045d3d47488877538b"
		}
		User.findById(_id)
			.then(user => {
				console.log(user)
				should.exist(user)
				done()
			})
	})
})