import MqttUser from "../src/MqttUser"
import User from "../src/user"
import MongoConnection from "../src/mongoose-connection"
var should = require("should")

describe("MqttUser", () => {
	describe("#aclTopicExists()", () => {
		it("Should return ture", (done) => {
			let _id = "599539045d3d47488877538d"
			let email = "test07@gmail.com"
			User.findById(_id).populate("_mqttUser").exec()
			.then( user => {
				console.log(user)
				let topic = "device/smt_770/IGTST_405/#"
				user._mqttUser.aclTopicExists(topic).should.equal(true)
				done()
			})
		})
		// it("Should return false", done => {
		// 	let _id = "599539045d3d47488877538d"
		// 	User.findById(_id).populate("_mqttUser").exec()
		// 	.then( user => {
		// 		console.log(user)
		// 		let topic = "device/wifi_gateway/IGHUB_TEST07/#"
		// 		user._mqttUser.aclTopicExists(topic).should.equal(false)
		// 		done()
		// 	})
		// })
	})
})